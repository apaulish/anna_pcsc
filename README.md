# Programming Concepts in Scientific Computing

## MATH-458 / 4 credits

- *Teacher:* Anciaux Guillaume
- *Language:* English

## Practical information

- Lectures are on Wednesdays from *10:15* to *12:00* in [CM1 103](https://plan.epfl.ch/?room==CM%201%20103)
- Labs are on Fridays from *08:15* to *10:00* in [CM1 103](https://plan.epfl.ch/?room==CM%201%20103)
- Lectures are broadcasted live on [https://epfl.zoom.us/j/95443983888](https://epfl.zoom.us/j/95443983888)
- Lecture videos are accessible on [MATH-458 SwitchTube](https://tube.switch.ch/channels/ZORUChMZcT)
- Asking questions is possible on [https://piazza.com/epfl.ch/fall2021/math458](https://piazza.com/epfl.ch/fall2021/math458)
- Lecture slides are accessible on [this gitlab](https://gitlab.epfl.ch/anciaux/pcsc/-/tree/master/lectures/)


## Summary

The aim of this course is to provide the background in scientific computing. The class includes a brief introduction to basic programming in c++, it then focuses on object oriented programming and c++ specific programming techniques.

## Assessment methods

The students will be evaluated with oral evaluations based on a programming project at the end of the semester.

# Resources

Joe Pitt-Francis and Jonathan Whiteley, Guide to Scientific Computing in C++, Springer 2012 (https://link.springer.com/book/10.1007/978-1-4471-2736-9)

